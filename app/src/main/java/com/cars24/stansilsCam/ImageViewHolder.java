package com.cars24.stansilsCam;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

/*
 * @author Amrit.Kumar on 19/12/18.
 */

public class ImageViewHolder extends RecyclerView.ViewHolder
{
    ImageView imvImage,imv_tick;
    private Context mContext;
    private Activity activity;
    public ImageViewHolder(View itemView, Activity activity) {
        super(itemView);
        this.activity=activity;
        mContext = itemView.getContext();

    }

    public void setValues(final ImageModel mediaData, boolean showNoImage) {
        if (mediaData != null) {
            imvImage = itemView.findViewById(R.id.image_box);
            imv_tick = itemView.findViewById(R.id.imv_tick);
            imvImage.setImageResource(mediaData.image);
            if (mediaData.isImageCaptured)
                imv_tick.setVisibility(View.VISIBLE);
            else
                imv_tick.setVisibility(View.GONE);
        }
    }
}
