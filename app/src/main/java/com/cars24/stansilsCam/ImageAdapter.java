package com.cars24.stansilsCam;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;

/*
 * @author Amrit.Kumar on 19/12/18.
 */

public class ImageAdapter extends RecyclerView.Adapter<ImageViewHolder> {
    private final LayoutInflater mLayoutInflater;
    private ArrayList<ImageModel> mediaData;
    private String title;
    private int itemPos;
    private Activity activity;


    public ImageAdapter(Activity activity, ArrayList<ImageModel> data) {
        this.mediaData = data;
        this.activity = activity;
        mLayoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public ImageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = mLayoutInflater.inflate(R.layout.gallery_item, parent, false);
        return new ImageViewHolder(itemView, activity);
    }

    @Override
    public void onBindViewHolder(@NonNull ImageViewHolder holder, final int position) {
        if (mediaData != null) {
            final ImageModel imageModel = mediaData.get(position);
            if (imageModel != null && holder != null) {
                //in case of indexed images all images will be editable in rejection also
                holder.setValues(imageModel, true);
                //define view item click listener
                holder.itemView.setOnClickListener(v -> {
                    Bundle args = new Bundle();
                    args.putInt("pos", position);
                    args.putParcelableArrayList("data", mediaData);
//                    mainActivity.onEvent("currentPos", position);
                });
            }
        }
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull ImageViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
    }



    @Override
    public int getItemCount() {
        if (mediaData != null) {
            return mediaData.size();
        }
        return 0;
    }
}
