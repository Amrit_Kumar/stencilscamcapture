package com.cars24.stansilsCam;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * @author Amrit Kumar on 19/12/18.
 */
public class ImageModel implements Parcelable {
    int image;
    boolean isImageCaptured;


    public ImageModel() {
    }

    protected ImageModel(Parcel in) {
        image = in.readInt();
        isImageCaptured = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(image);
        dest.writeByte((byte) (isImageCaptured ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ImageModel> CREATOR = new Creator<ImageModel>() {
        @Override
        public ImageModel createFromParcel(Parcel in) {
            return new ImageModel(in);
        }

        @Override
        public ImageModel[] newArray(int size) {
            return new ImageModel[size];
        }
    };
}
